package com.example.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.response.StudentResponse;

@RestController  // This is the combination of the @restController and @responseBody
@RequestMapping("/api/student")
public class StudentController {

	@Value("${app.name:Shivu Kumar}")
	private String name;
	
	@RequestMapping("/get")
	public StudentResponse getStudent() {
		StudentResponse studentResponse = new StudentResponse(1,name, "Siddapura");
		return studentResponse;
	}
	
}
